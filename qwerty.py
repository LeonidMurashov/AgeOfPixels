import os

lines = []


def count_lines_in_folder(folder):
    global lines
    for name in os.listdir(folder):
        path = os.path.join(folder, name)
        if os.path.isdir(path):
            count_lines_in_folder(path)
        elif os.path.isfile(path) and path[-3:] == '.py' and name != 'qwerty.py':
            with open(path, 'r') as file:
                for line in file.readlines():
                    if line not in ['\n'] and '#' not in line and 'import' != line[:6] and 'from' != line[:4]:
                        lines.append(line)


count_lines_in_folder('.')

for i in lines:
    print(f'\'{i[:-1]}\'')

print()
print(f'lines number: {len(lines)}')
