from server.game import *
import unittest
import time


class LoadTests(unittest.TestCase):
    world = World()
    clock = pygame.time.Clock()
    players = [HumanPlayer(world, 'Player'), HumanPlayer(world, 'Player1')]

    def tearDown(self):
        self.world.alive_objects = []
        self.world.objects = []

    def test_render(self):
        # the game requires good fps (30) when the number of units does not exceed 500
        # creating 507 units
        for i in range(13):
            for j in range(13):
                self.world.create_man("Villager", self.players[0], [300 + i * 50, 300 + j * 50])
                self.world.create_man("Archer", self.players[0], [600 + i * 50, 300 + j * 50])
                self.world.create_man("Swordsman", self.players[0], [900 + i * 50, 300 + j * 50])
        fps_sum = 0
        for i in range(100):
            elapsed_time = self.clock.tick_busy_loop() / 1000
            time.sleep(0.0001)

            self.world.step(elapsed_time)
            self.world.get_information_about_players(self.players[0])
            self.world.get_information_about_players(self.players[1])

            fps_sum += int(self.clock.get_fps())

        # checking average fps
        self.assertGreater(fps_sum / 100, GREAT_FPS)

    def test_moving_render(self):
        # creating 507 units
        for i in range(13):
            for j in range(13):
                target = (random.randint(0, 500), random.randint(0, 500))
                first = self.world.create_man("Villager", self.players[0], [300 + i * 50, 300 + j * 50])
                second = self.world.create_man("Archer", self.players[0], [600 + i * 50, 300 + j * 50])
                third = self.world.create_man("Swordsman", self.players[0], [900 + i * 50, 300 + j * 50])
                first.go_to(target)
                second.go_to(target)
                third.go_to(target)
        fps_sum = 0
        for i in range(100):
            elapsed_time = self.clock.tick_busy_loop() / 1000
            time.sleep(0.0001)

            self.world.step(elapsed_time)
            self.world.get_information_about_players(self.players[0])
            self.world.get_information_about_players(self.players[1])

            fps_sum += int(self.clock.get_fps())

        # checking average fps
        self.assertGreater(fps_sum / 100, GREAT_FPS)

    def test_dying_render(self):
        # creating 507 units
        for i in range(13):
            for j in range(13):
                first = self.world.create_man("Villager", self.players[0], [300 + i * 50, 300 + j * 50])
                second = self.world.create_man("Archer", self.players[0], [600 + i * 50, 300 + j * 50])
                third = self.world.create_man("Swordsman", self.players[0], [900 + i * 50, 300 + j * 50])
                first._health = 0
                second._health = 0
                third._health = 0
        fps_sum = 0
        for i in range(100):
            elapsed_time = self.clock.tick_busy_loop() / 1000
            time.sleep(0.0001)

            self.world.step(elapsed_time)
            self.world.get_information_about_players(self.players[0])
            self.world.get_information_about_players(self.players[1])

            fps_sum += int(self.clock.get_fps())

        # checking average fps
        self.assertGreater(fps_sum / 100, GREAT_FPS)
