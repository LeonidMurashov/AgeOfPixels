from server.game import *
import unittest
from server.world import *


class UnitTests(unittest.TestCase):
    world = World()
    clock = pygame.time.Clock()
    players = [HumanPlayer(world, 'Player'), HumanPlayer(world, 'Player1')]

    def tearDown(self):
        self.world.alive_objects = []
        self.world.objects = []

    def test_simple(self):
        self.assertEqual(1, 1)

    def test_moving(self):
        test_swordsman = self.world.create_man('Swordsman', self.players[0], (0, 0))

        for i in range(10):
            target = (random.randint(0, 500), random.randint(0, 500))
            test_swordsman.go_to(target)

            while test_swordsman._moving:
                elapsed_time = self.clock.tick_busy_loop() / 100
                self.world.step(elapsed_time)

            self.assertAlmostEqual(target[0], round(test_swordsman.get_bbox().x), delta=2)
            self.assertAlmostEqual(target[1], round(test_swordsman.get_bbox().y), delta=2)
        print('Units can move!')

    def test_chasing(self):
        unit = self.world.create_man('Swordsman', self.players[0], (0, 0))
        enemy = self.world.create_man('Swordsman', self.players[1], (0, 0))

        for j in range(10):
            enemy._bbox.x = random.randint(0, 100)
            enemy._bbox.y = random.randint(0, 100)
            for i in range(STEPS_TO_FIND_ENEMY + 1):
                elapsed_time = self.clock.tick_busy_loop() / 100
                self.world.step(elapsed_time)
            self.assertEqual(unit._chasing_object, enemy)
            self.assertEqual(enemy._chasing_object, unit)
        print('Units can chase!')

    def test_death(self):
        test_unit = self.world.create_man('Swordsman', self.players[0], (0, 0))
        test_unit._health = 0
        for i in range(60):
            elapsed_time = self.clock.tick_busy_loop() / 100
            self.world.step(elapsed_time)
        self.assertEqual(test_unit.is_dead(), True)
        print('Units can die!')
