import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'tests'))

from server.game import *
import unittest


class StressTests(unittest.TestCase):
    world = World()
    clock = pygame.time.Clock()
    players = [HumanPlayer(world, 'Player'), HumanPlayer(world, 'Player1')]

    def test_max_units_number(self):
        avg_fps = GREAT_FPS
        new_units_number = 200
        iterations_number = 100
        while avg_fps >= GREAT_FPS:
            fps_sum = 0
            for i in range(new_units_number):
                self.world.create_man("Villager", self.players[0], [random.randint(0, 1000), random.randint(0, 1000)])
            for i in range(iterations_number):
                elapsed_time = self.clock.tick_busy_loop() / 1000

                self.world.step(elapsed_time)
                self.world.get_information_about_players(self.players[0])
                self.world.get_information_about_players(self.players[1])

                fps_sum += int(self.clock.get_fps())

            avg_fps = fps_sum / iterations_number
        print('Max units number = ', len(self.world.objects) - new_units_number)

