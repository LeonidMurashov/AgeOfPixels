FROM python:3.6

COPY . /game/AgeOfPixels
WORKDIR /game/AgeOfPixels

EXPOSE 54123

RUN pip3 install -r requirements.txt
CMD python3 server/main.py