import contextlib

with contextlib.redirect_stdout(None):
    import pygame

import pygame.locals
from server.world import *
from server.player import HumanPlayer
from server.network_server_manager import NetworkServerManager
import time


def lobby(network_manager):
    print('Waiting for self.players to connect...')
    while network_manager.players_number is None or \
          len(network_manager.players) < network_manager.players_number or \
          any(player.player_name == 'NOT READY' for player in network_manager.players):
        network_manager.Update()
        time.sleep(0.01)

    print('Starting the game.')
    network_manager.SendGameStarted()


class Game:
    def __init__(self, network_manager):
        self.network_manager = network_manager
        lobby(network_manager)

        self.world = World()

        self.players = []
        start_position = [300, 300]
        for player in self.network_manager.players:
            self.players.append(
                HumanPlayer(self.world, player.player_name, start_position[0], start_position[1])
            )
            start_position[0] += 2000
            start_position[1] += random.randint(-1000, 1000)

        self.players.append(HumanPlayer(self.world, 'Bot', start_position[0], start_position[1]))

        self.players_dict = {player.name: player for player in self.players}



        self.world.create_ore()
        for player in self.players:
            player.act()
            player.create_army(1)

        self.clock = pygame.time.Clock()
            

    def step(self):
        elapsed_time = self.clock.tick_busy_loop() / 1000

        self.network_manager.Update()
        events = self.network_manager.GetEvents()

        for event in events:
            if event['player_name'] not in self.players_dict:
                continue
            self.players_dict[event['player_name']].push_events(event['events'])

        # self.players_dict['leo'].push_events([{'name': 'offset_camera', 'params': [[1, 1]]}])

        self.world.step(elapsed_time)
        # print(clock.get_fps())

        for player in self.players:
            if player.name == 'Bot':
                continue
            frame_data = self.world.get_information_about_players(player)
            self.network_manager.SendFrameData(frame_data, player.name)

        time.sleep(0.02)

        # Close game if everyone left
        if len(self.network_manager.players) == 0:
            return False
        return True
