from server.factory.building_factory import *
from server.factory.unit_factory import *
from server.factory.ore import *
from typing import List
from server.monitor import *


class DebugObject(GameObject):
    def __init__(self, screen, x, y, color):
        self.s = pygame.Surface((5, 5))
        self.s.fill(color)
        self.x = x
        self.y = y
        self.screen = screen

    def step(self, delta_t):
        pass

    def render(self):
        self.screen.blit(self.s, (self.x, self.y))


def list_to_rect(lst):
    return pygame.Rect(*lst)


class World:
    def __init__(self):
        # No idea what c is
        c = [1920, 1080]
        self.bbox = CircleBBox(c[0] / 2, c[1] / 2, c[1] / 2)
        self.building_factory = BuildingFactory(self)
        self.man_factory = UnitFactory(self)
        self.monitor = Monitor()
        self.players = []


        self.objects: List[GameObject] = []
        self.alive_objects: List[GameObject] = []
        self.ore_objects: List[Ore] = []
        self.selected_objects: List[GameObject] = []
        self.debug_objects: List[GameObject] = []
        self.dying_objects: List[GameObject] = []


    def step(self, elapsed_time):
        for obj in self.alive_objects:
            # Filtering out dying objects
            if obj.is_dying():
                self.alive_objects.remove(obj)
                self.dying_objects.append(obj)
                try:
                    self.selected_objects.remove(obj)
                except ValueError:
                    pass
            obj.step(elapsed_time)

        for obj in self.dying_objects:
            if obj.is_dead():
                self.dying_objects.remove(obj)
                self.objects.remove(obj)
            obj.step(elapsed_time)

        for obj in self.ore_objects:
            if obj.is_dead():
                self.objects.remove(obj)
                self.ore_objects.remove(obj)

    def create_man(self, name, player, pos):
        man = self.man_factory.create_man(name, player, pos)
        self.objects.append(man)
        self.alive_objects.append(man)
        return man

    def create_building(self, name, player, pos):
        building = self.building_factory.create_building(name, player, pos)
        self.objects.append(building)
        self.alive_objects.append(building)
        return building

    def create_ore(self):
        for j in range(20):
            self.create_ore_group('wood')
            self.create_ore_group('stone')
            self.create_ore_group('gold')

    def create_ore_group(self, ore_type):
        x = random.randint(-4000, 4000)
        y = random.randint(-4000, 4000)
        for i in range(5):
            ore = Ore(self, (x + random.randint(0, 100), y + random.randint(0, 100)), ore_type)
            self.objects.append(ore)
            self.ore_objects.append(ore)

    def move_selected(self, player, mouse_pos):
        if len(self.selected_objects) == 0:
            return

        selected_objects = []
        for obj in self.selected_objects:
            if obj.get_owner() == player:
                selected_objects.append(obj)

        if len(selected_objects) == 0:
            return

        # Creating target position rectangle
        units_num = len(selected_objects)
        center = (sum(obj.get_bbox().x for obj in selected_objects) / units_num,
                  sum(obj.get_bbox().y for obj in selected_objects) / units_num)
        angle = math.atan2(mouse_pos[1] - center[1],
                           mouse_pos[0] - center[0])

        position_distance = sum(obj.object_width for obj in selected_objects) / len(selected_objects)
        positions = []

        for j in range(int(math.ceil(math.sqrt(units_num))) - 1, -1, -1):
            for i in range(int(math.ceil(math.sqrt(units_num)))):
                x = position_distance * ((j - int(math.ceil(math.sqrt(units_num))) / 2) * math.cos(angle) -
                                         (i - int(math.ceil(math.sqrt(units_num))) / 2) * math.sin(angle)) \
                    + mouse_pos[0]
                y = position_distance * ((j - int(math.ceil(math.sqrt(units_num))) / 2) * math.sin(angle) +
                                         (i - int(math.ceil(math.sqrt(units_num))) / 2) * math.cos(angle)) \
                    + mouse_pos[1]
                positions.append([x, y])
        # self.debug_objects.clear()
        # self.debug_objects.append(DebugObject(self.screen, positions[0][0], positions[0][1], (255, 25, 255)))

        # Move units
        for i, obj in enumerate(selected_objects):
            if isinstance(obj, Unit):
                obj.go_to(positions[i])

    def select_objects(self, player, rect, camera_coordinates):
        rect = list_to_rect(rect)
        # menu click
        if rect.y > USUAL_SCREEN_SIZE[1] - 160:
            return

        rect.x, rect.y = rect.x - camera_coordinates[0], rect.y - camera_coordinates[1]

        obj_to_remove = []
        for obj in self.selected_objects:
            if obj.get_owner() == player:
                obj._is_selected = False
                obj_to_remove.append(obj)

        for i in obj_to_remove:
            self.selected_objects.remove(i)

        if rect.w < 2 and rect.h < 2:
            # Click case
            mouse_bbox = CircleBBox(rect.x, rect.y, PIXEL_SCALE * 2)
            min_dist = MAX_POSSIBLE_DIST
            min_object = None
            for obj2 in self.alive_objects:
                dist = mouse_bbox.distance_to(obj2.get_bbox())
                if min_dist > dist:
                    min_dist = dist
                    min_object = obj2
            if min_dist <= min_object.object_width and min_object.get_owner() == player:
                self.selected_objects.append(min_object)
                min_object.set_is_selected(True)
        else:
            # Rect case
            for obj in self.alive_objects:
                if rect.collidepoint(obj.get_bbox().x, obj.get_bbox().y) and obj.get_owner() == player:
                    self.selected_objects.append(obj)
                    obj.set_is_selected(True)

    def request_move(self, obj, bbox1):
        '''
        if not bbox1.is_collision(self.bbox) and \
                obj.get_bbox().is_collision(self.bbox):
            return False
        for i in self.alive_objects:
            bbox2 = i.get_bbox()
            if i != obj and \
                    bbox1.is_collision(bbox2) and \
                    not obj.get_bbox().is_collision(bbox2):
                return False
        '''
        return True

    def find_closest_enemy(self, obj: Unit):
        obj_owner = obj.get_owner()
        min_dist = MAX_POSSIBLE_DIST
        min_object = None
        for obj2 in self.alive_objects:
            if obj_owner != obj2.get_owner():
                dist = obj.get_bbox().distance_to(obj2.get_bbox())
                if min_dist > dist:
                    min_dist = dist
                    min_object = obj2
        return min_object if min_dist <= obj.get_line_of_sight() else None

    def find_closest_ore(self, obj):
        min_dist = MAX_POSSIBLE_DIST
        min_object = None
        for obj2 in self.objects:
            if isinstance(obj2, Ore) and not obj2.is_mining():
                dist = obj.get_bbox().distance_to(obj2.get_bbox())
                if min_dist > dist:
                    min_dist = dist
                    min_object = obj2
        return min_object if min_dist <= obj.get_line_of_sight() else None

    def find_closest_building(self, obj):
        min_dist = MAX_POSSIBLE_DIST
        min_object = None
        for obj2 in self.objects:
            if isinstance(obj2, BuildingWarrior) and obj2.get_owner() == obj.get_owner():
                dist = obj.get_bbox().distance_to(obj2.get_bbox())
                if min_dist > dist:
                    min_dist = dist
                    min_object = obj2
        return min_object

    def remove_selected(self):
        for obj in self.selected_objects:
            obj.set_health(obj.get_health() - 10)

    def check_ore_collision(self, ore):
        for i in self.selected_objects:
            bbox1 = i.get_bbox()
            if ore.get_bbox().is_collision(bbox1):
                return True

    def get_selected_type(self, player):
        for obj in self.selected_objects:
            if obj.get_owner() == player:
                return obj

    def get_information_about_players(self, target_player):
        return self.monitor.extract_information(self.objects, self.players, target_player)
