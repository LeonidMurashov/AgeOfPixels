import random
from typing import Tuple
from server.menu import *


class Player:
    _name: str
    _age: str
    color: Tuple[int]
    speed_bonus = 0
    damage_bonus_archer = 0
    damage_bonus_swordsman = 0
    mine_bonus = 0
    menu = dict()

    def __init__(self, world, name, start_x=0, start_y=0):
        self.camera_coordinates = [200, 200]
        self.world = world
        self._name = name
        self.s_p = [start_x, start_y]
        self.color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        self._menu = Menu(world, self)
        self.resources = {"gold": 100, "food": 200, "wood": 0, "stone": 0}

    def get_player_info(self):
        player_information = {
            'type': 'player_info',
            'name': self.name,
            'resources': self.resources,
            'menu': self._menu.get_menu_information()
        }
        return player_information

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name


class AIPlayer(Player):
    pass


class HumanPlayer(Player):
    def __init__(self, world, name, start_x=0, start_y=0):
        super().__init__(world, name, start_x, start_y)

    def selection_rect_finished(self, rect):
        self.world.select_objects(self, rect, self.camera_coordinates)

    def right_click(self, mouse_pos):
        mouse_pos = (mouse_pos[0] - self.camera_coordinates[0], mouse_pos[1] - self.camera_coordinates[1])
        self.world.move_selected(self, mouse_pos)

    def left_click(self, mouse_pos):
        # Subtract camera shift to c
        self._menu.left_click(mouse_pos)

    def act(self):
        for i in range(4):
            for j in range(4):
                self.world.create_man("Villager", self, [self.s_p[0] + i * 50, self.s_p[1] + j * 50])
                self.world.create_man("Archer", self, [self.s_p[0] + 300 + i * 50, self.s_p[1] + j * 50])
                self.world.create_man("Swordsman", self, [self.s_p[0] + 600 + i * 50, self.s_p[1] + j * 50])
        self.world.create_building("BuildingWarrior", self, [self.s_p[0] + 900, self.s_p[1]])

    def create_army(self, num):
        for i in range(num):
            self.world.create_man('CarWarrior', self, [self.s_p[0] + random.randint(100, 100) - 400, self.s_p[1] + random.randint(100, 100) - 400])

    def delete_button(self):
        self.camera_coordinates[0] += 1
        # self.world.remove_selected()

    def offset_camera(self, d):
        self.camera_coordinates = [self.camera_coordinates[0] + d[0], self.camera_coordinates[1] + d[1]]

    def process_events(self):
        for event in self.events:
            print(event)
            self.__getattribute__(event['name'])(*(event['params']))
        self.events.clear()

    def push_events(self, events):
        self.events = events
        self.process_events()
