import sys
from time import sleep
from weakref import WeakKeyDictionary

from PodSixNet.Server import Server
from PodSixNet.Channel import Channel


class ClientChannel(Channel):
    """
    This is the server representation of a single connected client.
    """

    def __init__(self, *args, **kwargs):
        self.player_name = 'NOT READY'
        Channel.__init__(self, *args, **kwargs)

    def Close(self):
        self._server.DelPlayer(self)

    ##############################
    # Network specific callbacks #
    ##############################

    def Network_events(self, data):
        data['player_name'] = self.player_name
        # print(data)
        self._server._events.append(data)

    def Network_player_name(self, data):
        self.player_name = data['player_name']
        self._server.player_name_to_player[data['player_name']] = self
        self._server.SendToAll({'action': 'player_connected', 'player_name': self.player_name})

    def Network_set_players_number(self, data):
        self._server.players_number = data['players_number']

class NetworkServerManager(Server):
    channelClass = ClientChannel
    _events = []
    players_number = None

    def __init__(self, *args, **kwargs):
        Server.__init__(self, *args, **kwargs)
        self.players = WeakKeyDictionary()
        self.player_name_to_player = dict()
        print('Server launched')

    def Connected(self, channel, addr):
        self.AddPlayer(channel)

    def AddPlayer(self, player):
        print('New Player' + str(player.addr))
        self.players[player] = True
        print('players', [p for p in self.players])

    def DelPlayer(self, player):
        print('Deleting Player' + str(player.addr))
        del self.players[player]

    def SendGameStarted(self):
        self.SendToAll({'action': 'game_started'})

    def SendFrameData(self, data, target_player_name):
        player = self.player_name_to_player[target_player_name]
        # print(data)
        player.Send({'action': 'frame_data', 'data': data})

    def SendToAll(self, data):
        for p in self.players:
            p.Send(data)

    def Update(self):
        self.Pump()

    def GetEvents(self):
        return self._events
