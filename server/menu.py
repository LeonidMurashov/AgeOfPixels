from server.upgrades_reader import *
from typing import Tuple
from typing import List
import pygame


def list_to_rect(lst):
    return pygame.Rect(*lst)


class Menu:
    coordinates: Tuple[int]

    def __init__(self, world, player):
        self.world = world
        self._player = player
        self.upgrade_buttons = []
        self.action_buttons = []
        self.create_buttons()

    def create_buttons(self):
        upgrades = read_upgrades()
        button_coordinates = [380, 950]
        for button in upgrades:
            self.upgrade_buttons.append(Button(button, button_coordinates))
            button_coordinates[0] += 220

    def get_menu_information(self):
        selected_object = self.world.get_selected_type(self._player)

        selected_type_name = ''
        if selected_object is not None:
            selected_type_name = selected_object.get_name()

        information = {'selected_type': selected_type_name,
                       'upgrades': self.get_upgrades(selected_object),
                       'actions': self.get_actions(selected_object)}
        return information

    def get_upgrades(self, selected_object):
        upgrades = []
        if selected_object is not None and selected_object.get_name() == 'building warrior':
            for i in self.upgrade_buttons:
                if i.cost['gold'] <= self._player.resources['gold'] \
                        and i.cost['food'] <= self._player.resources['food'] \
                        and i.cost['wood'] <= self._player.resources['wood'] \
                        and i.cost['stone'] <= self._player.resources['stone']:
                    upgrades.append(i.can_push())
                    i.can_be_pushed = True
                else:
                    upgrades.append(i.cant_push())
        return upgrades

    def get_actions(self, selected_object):
        pass

    def left_click(self, mouse_pos):
        for button in self.upgrade_buttons:
            if button.rect.collidepoint(mouse_pos) and button.can_be_pushed:
                button.callback(self._player)
                return



class Button:
    def __init__(self, button, location):
        self.influenced = button['influenced_units']
        self.size = [210, 110]
        self.cost = button['cost']
        self.name = button['name']
        self.bonus = button['bonuses']
        self.x = location[0]
        self.y = location[1]
        self.rect = list_to_rect([self.x, self.y, self.size[0], self.size[1]])
        self.can_be_pushed = False

    def callback(self, player):
        for bonus in self.bonus:
            if bonus == 'damage':
                if 'Swordsman' in self.influenced:
                    player.damage_bonus_swordsman += self.bonus['damage']
                if 'Archer' in self.influenced:
                    player.damage_bonus_archer += self.bonus['damage']
            elif bonus == 'speed':
                player.speed_bonus += self.bonus['speed']
            elif bonus == 'mine':
                player.mine_bonus += self.bonus['mine']
        player.resources['gold'] -= self.cost['gold']
        player.resources['wood'] -= self.cost['wood']
        player.resources['food'] -= self.cost['food']
        player.resources['stone'] -= self.cost['stone']
        self.can_be_pushed = False

    def can_push(self):
        information = {'type': 'bonus', 'name': self.name, 'x': self.x, 'y': self.y, 'w': self.size[0],
                       'h': self.size[1], 'color': (0, 0, 0)}
        return information

    def cant_push(self):
        information = {'type': 'bonus', 'name': self.name, 'x': self.x, 'y': self.y, 'w': self.size[0],
                       'h': self.size[1], 'color': (200, 200, 200)}
        return information
