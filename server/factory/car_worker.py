from server.factory.unit import *


class CarWorker(Unit):
    _name = 'car worker'
    _sprite_name = 'car.png'
    _speed = 150
    _object_width = 150

    def take_an_object(self):
        pass

    def get_object_info(self):
        objects = list()

        if self._is_selected:
            ellipsis_info = {
                'type': 'selection_ellipsis',
                'x': self.get_x(),
                'y': self.get_y() + self._sprite_offset,
                'width_ratio_with_image': 1.2
            }
            objects.append(ellipsis_info)

        main_picture_info = {
            'type': 'picture',
            'x': self._bbox.x,
            'y': self._bbox.y + self._sprite_offset,
            'sprite_name': self.get_sprite_name(),
            'steps': self._death_steps
        }
        objects.append(main_picture_info)

        if self._is_selected:
            health = {
                'type': 'health_bar',
                'x': self.get_x(),
                'y': self.get_y(),
                'health_ratio': self._health / self._max_health,
            }
            objects += [health]

        information = {
            'owner_color': self.get_owner().color,
            'depth': self.get_y(),
            'objects': objects,
            'main_sprite_name': self._sprite_name
        }
        return information
