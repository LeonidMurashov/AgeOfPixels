from server.factory.unit import *


class Archer(Unit):
    _sprite_name = 'builder.bmp'
    _speed = 150
    _damage = 10
    _name = 'archer'

    def get_damage(self):
        return self._damage + self._owner.damage_bonus_archer

    def build(self):
        pass

