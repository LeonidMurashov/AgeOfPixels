from server.factory.unit import *
from server.factory.building_warrior import *
from server.factory.ore import *
from server.factory.useful_functions import time_has_come


class Villager(Unit):
    _name = 'villager'
    _sprite_name = 'worker.bmp'
    _speed = 100
    _line_of_sight = 200
    _line_of_taking = 10
    _mine_speed = 1
    _chasing_object: Ore

    def __init__(self, world, coordinates, owner):
        super(Villager, self).__init__(world, coordinates, owner)
        self._closest_building_coordinates = []
        self._ore_coordinates = []
        self._ore_type = ''

    def step(self, delta_t):
        if self._health > 0:
            if self._state_stack[-1] == 'going_to':
                self._moving = True
                self.animate_go_to(delta_t)

            if self._state_stack[-1] == 'idle':
                if time_has_come(STEPS_TO_CHECK_ORE_COLLISION, self._step_counter):
                    ore = self._world.find_closest_ore(self)
                    if ore is not None:
                        self._chasing_object = ore
                        self._ore_coordinates = [self._chasing_object.get_bbox().x, self._chasing_object.get_bbox().y]
                        self._moving = True
                        self._state_stack.append('finding')

            if self._state_stack[-1] == 'finding':
                if self._chasing_object is not None:
                    # Check if still visible
                    if self._chasing_object.get_bbox().distance_to(
                            self.get_bbox()) > self._line_of_sight or self._chasing_object.is_mining():
                        self._chasing_object = None
                        self._moving = False
                        del self._state_stack[-1]
                    # Check if can take
                    elif self._chasing_object.get_bbox().distance_to(
                            self.get_bbox()) < self._line_of_taking and not self._chasing_object.is_mining():
                        self._state_stack.append('taking')
                        self._target = [self._chasing_object.get_bbox().x, self._chasing_object.get_bbox().y]
                    elif not self._chasing_object.is_mining():
                        self._target = [self._chasing_object.get_bbox().x, self._chasing_object.get_bbox().y]
                        self.animate_go_to(delta_t)
                    else:
                        del self._state_stack[-1]
                else:
                    self._moving = False
                    del self._state_stack[-1]

            if self._state_stack[-1] == 'taking':
                # if can still take
                if self._chasing_object is not None and self._chasing_object.get_bbox().distance_to(
                        self.get_bbox()) < self._line_of_taking:
                    self.take_an_object()
                    if self._chasing_object.is_dead():
                        self._ore_type = self._chasing_object.get_type()
                        del self._state_stack[-1]
                        self._state_stack.append('find_base')
                else:
                    self._chasing_object = None
                    del self._state_stack[-1]

            if self._state_stack[-1] == 'find_base':
                closest_building = self._world.find_closest_building(self)
                self._closest_building_coordinates = [closest_building.get_bbox().x, closest_building.get_bbox().y]
                del self._state_stack[-1]
                self._state_stack.append('going_to_base')

            if self._state_stack[-1] == 'going_to_base':
                self._moving = True
                self._target = self._closest_building_coordinates
                self.animate_go_to(delta_t)
                target_bbox = CircleBBox(self._target[0], self._target[1], 0)
                if self._bbox.distance_to(target_bbox) < self.get_speed() * delta_t:
                    self._target = None
                    del self._state_stack[-1]
                    self._target = self._ore_coordinates
                    self.get_owner().resources[self._ore_type] += 10
                    self._state_stack.append('going_to')

        else:
            if not self._dying:
                self._dying = True
            self._death_steps -= 1
        self._time += delta_t
        self._step_counter += 1

    def get_mine_speed(self):
        return self._mine_speed + self.get_owner().mine_bonus

    def take_an_object(self):
        self._chasing_object.take(self.get_mine_speed())

    def get_object_info(self):
        information = super(Villager, self).get_object_info()
        if 'going_to_base' in self._state_stack:
            ore = {'type': 'picture', 'x': self.get_bbox().x + 10, 'y': self.get_bbox().y - 20,
                   'sprite_name': self._ore_type + '.png'}
            information['objects'].append(ore)
        return information
