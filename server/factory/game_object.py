from server.configs import *
from server.bbox import BBox
from server.player import Player

import pygame


class GameObject(ABC):
    _owner: Player
    _is_selected: bool = False
    _image: pygame.Surface
    _screen: pygame.Surface
    _bbox: BBox
    _health: float
    _max_health: float
    _dying = False
    _object_width: float

    @abstractmethod
    def step(self, delta_t):
        pass

    @abstractmethod
    def is_dead(self):
        pass

    @abstractmethod
    def get_object_info(self):
        pass

    @property
    def object_width(self):
        return self._object_width

    def get_owner(self):
        return self._owner

    def get_bbox(self):
        return self._bbox

    def set_is_selected(self, state):
        self._is_selected = state

    def get_is_selected(self):
        return self._is_selected

    def is_dying(self):
        return self._dying

    def get_health(self):
        return self._health

    def set_health(self, val):
        self._health = val
