from server.factory.building_warrior import *
from server.factory.building_worker import *


class BuildingFactory:
    _name = 'building factory'

    def __init__(self, world):
        self._world = world

    def create_building(self, name, player, coordinates):
        if name == "BuildingWorker":
            return BuildingWorker(self._world, coordinates, player)
        if name == "BuildingWarrior":
            return BuildingWarrior(self._world, coordinates, player)
