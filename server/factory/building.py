from server.factory.game_object import *
from server.bbox import *
import random


class Building(GameObject, ABC):
    _name: str
    _image: pygame.Surface
    _sprite_name: str
    _object_width = 50
    _max_health = 4000

    def __init__(self, world, coordinates, owner):
        self._world = world
        self._owner = owner

        self._bbox = CircleBBox(coordinates[0], coordinates[1], 3)
        self._health = 4000

    def step(self, delta_t):
        pass

    def is_dead(self):
        return self._health < 0

    def get_x(self):
        return self._bbox.x

    def get_y(self):
        return self._bbox.y

    def get_object_info(self):
        picture_info = {'type': 'picture', 'x': self._bbox.x,
                        'y': self._bbox.y,
                        'sprite_name': self._sprite_name}
        objects = [picture_info]
        if self._is_selected:
            health = {
                'type': 'health_bar',
                'x': self.get_x(),
                'y': self.get_y(),
                'health_ratio': self._health / self._max_health,
            }
            objects += [health]
        information = {
            'owner_color': self.get_owner().color,
            'owner_name': self.get_owner().get_player_info()['name'],
            'depth': self.get_y(),
            'objects': objects,
            'main_sprite_name': self._sprite_name
        }

        return information

    def get_name(self):
        return self._name

    def is_dying(self):
        return self._health < 0
