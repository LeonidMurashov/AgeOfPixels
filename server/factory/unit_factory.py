from server.factory.archer import *
from server.factory.swordsman import *
from server.factory.villager import *
from server.factory.car_warrior import *
from server.factory.car_worker import *


class UnitFactory:
    def __init__(self, world):
        self._world = world

    def create_man(self, name, player, coordinates):
        if name == "Villager":
            return Villager(self._world, coordinates, player)
        if name == "Swordsman":
            return Swordsman(self._world, coordinates, player)
        if name == "Archer":
            return Archer(self._world, coordinates, player)
        if name == "CarWorker":
            return CarWorker(self._world, coordinates, player)
        if name == "CarWarrior":
            return CarWarrior(self._world, coordinates, player)


