from server.factory.game_object import *
from server.bbox import *
from copy import deepcopy
from server.configs import NUMBER_THAT_IS_BIGGER_THAN_ANY_STEP_COUNTER
import random


class Unit(GameObject, ABC):
    _name: str
    _speed: int
    _line_of_sight = 200
    _target = None
    _moving = False
    _chasing_object: GameObject = None
    _sprite_offset = 0
    _bbox: CircleBBox
    _step_counter = random.randint(0, NUMBER_THAT_IS_BIGGER_THAN_ANY_STEP_COUNTER)
    _max_health = 100
    _health = _max_health
    _death_steps = 60
    _sprite_name: str
    _time = 0
    _command_stack = []
    _dying = False
    _unit_width: float = 30
    _object_width = 30

    def __init__(self, world, coordinates, owner):
        self._world = world
        self._owner = owner

        self._state_stack = ['idle']

        self._bbox = CircleBBox(coordinates[0], coordinates[1], 3)
        self._health = 100

    def get_speed(self):
        return self._speed + self._owner.speed_bonus

    def get_damage(self):
        pass

    def animate_go_to(self, delta_t):
        if self._moving and self._target is not None:
            new_bbox = deepcopy(self._bbox)
            new_bbox.x += self.get_speed() * math.sin(
                math.atan2(self._target[0] - self._bbox.x,
                           self._target[1] - self._bbox.y)) * delta_t
            new_bbox.y += self.get_speed() * math.cos(
                math.atan2(self._target[0] - self._bbox.x,
                           self._target[1] - self._bbox.y)) * delta_t

            # Moving ended
            target_bbox = CircleBBox(self._target[0], self._target[1], 0)
            if self._bbox.distance_to(target_bbox) < self.get_speed() * delta_t:
                self._moving = False
                if self._state_stack[-1] == 'going_to':
                    del self._state_stack[-1]

            # Check for collisions with world bounds
            if self._world.request_move(self, new_bbox):
                self._bbox = new_bbox

                # Jumping animation
                self._sprite_offset = 2 * ((self._time * 20) % 5)

    def step(self, delta_t):
        if self._health > 0:
            if self._moving:
                if self._chasing_object is not None:
                    # Check if still visible
                    if self._chasing_object.get_bbox().distance_to(self._bbox) > self._line_of_sight:
                        self._chasing_object = None
                        return
                    self._target = [self._chasing_object.get_bbox().x, self._chasing_object.get_bbox().y]
                self.animate_go_to(delta_t)
            else:
                if self._step_counter == STEPS_TO_FIND_ENEMY:
                    enemy = self._world.find_closest_enemy(self)
                    if enemy is not None:
                        self._chasing_object = enemy
                        self._moving = True
        else:
            # Dying phase on
            if not self._dying:
                # TODO: Death sound, etc..
                self._dying = True
            self._death_steps -= 1
        self._time += delta_t

        # DEPRECATE: do not use step_counter to count steps this way
        self._step_counter = 0 if self._step_counter == 10 else self._step_counter + 1

    def go_to(self, target):
        self._target = target
        self._chasing_object = None
        self._moving = True
        self._state_stack.append('going_to')

    def get_y(self):
        return self._bbox.y

    def get_x(self):
        return self._bbox.x

    def get_owner(self):
        return self._owner

    def get_sprite_name(self):
        return self._sprite_name

    def get_line_of_sight(self):
        return self._line_of_sight

    def is_dead(self):
        return self._death_steps <= 0

    def get_object_info(self):
        objects = list()

        if self._is_selected:
            ellipsis_info = {
                'type': 'selection_ellipsis',
                'x': self.get_x(),
                'y': self.get_y() + self._sprite_offset,
                'width_ratio_with_image': 3
            }
            objects.append(ellipsis_info)

        main_picture_info = {
            'type': 'picture',
            'x': self._bbox.x,
            'y': self._bbox.y + self._sprite_offset,
            'sprite_name': self.get_sprite_name(),
            'steps': self._death_steps
        }
        objects.append(main_picture_info)

        if self._is_selected:
            health = {
                'type': 'health_bar',
                'x': self.get_x(),
                'y': self.get_y(),
                'health_ratio': self._health / self._max_health,
            }
            objects += [health]

        information = {
            'owner_name': self.get_owner().get_player_info()['name'],
            'owner_color': self.get_owner().color,
            'depth': self.get_y(),
            'objects': objects,
            'main_sprite_name': self._sprite_name
        }
        return information

    def get_name(self):
        return self._name
