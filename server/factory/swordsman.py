from server.factory.unit import *
from server.factory.useful_functions import time_has_come


class Swordsman(Unit):
    _name = 'swordsman'
    _sprite_name = 'warrior.bmp'
    _speed = 150
    _damage = 10
    _line_of_attack = 20
    _line_of_sight = 200

    def step(self, delta_t):
        if self._health > 0:
            if self._state_stack[-1] == 'going_to':
                self._moving = True
                self.animate_go_to(delta_t)
            if self._state_stack[-1] == 'idle':
                if time_has_come(STEPS_TO_FIND_ENEMY, self._step_counter):
                    enemy = self._world.find_closest_enemy(self)
                    if enemy is not None:
                        self._chasing_object = enemy
                        self._moving = True
                        self._state_stack.append('chasing')
            if self._state_stack[-1] == 'chasing':
                if self._chasing_object is not None:
                    # Check if still visible
                    if self._chasing_object.get_bbox().distance_to(self.get_bbox()) > self._line_of_sight:
                        self._chasing_object = None
                        del self._state_stack[-1]
                    # Check if can attack
                    elif self._chasing_object.get_bbox().distance_to(self.get_bbox()) < self._line_of_attack:
                        self._state_stack.append('attack')
                        self._target = [self._chasing_object.get_bbox().x, self._chasing_object.get_bbox().y]
                    else:
                        self._target = [self._chasing_object.get_bbox().x, self._chasing_object.get_bbox().y]
                        self.animate_go_to(delta_t)
                else:
                    del self._state_stack[-1]
            if self._state_stack[-1] == 'attack':
                # if can still attack
                if self._chasing_object is not None and self._chasing_object.get_bbox().distance_to(
                        self.get_bbox()) < self._line_of_attack:
                    self.attack()
                else:
                    del self._state_stack[-1]
        else:
            if not self._dying:
                self._dying = True
            self._death_steps -= 1
        self._time += delta_t
        self._step_counter += 1

    def get_damage(self):
        return self._damage + self._owner.damage_bonus_swordsman

    def attack(self):
        self._chasing_object._health -= self.get_damage()*0.1
        if self._chasing_object.get_health() < 0:
            self._chasing_object = None
            del self._state_stack[-1]
