from server.factory.game_object import *
from server.bbox import *
import os
import random


class Ore(GameObject):
    _moving = False
    _chasing_object: GameObject = None
    _bbox: CircleBBox
    _health = 100
    _is_mining = False

    def __init__(self, world, coordinates, ore_type):
        self._world = world
        self._step_counter = 0
        self._bbox = CircleBBox(coordinates[0], coordinates[1], 1)
        self._animation = [0, 0]
        self._name = ore_type
        self._sprite_name = ore_type + '.png'

    def get_object_info(self):
        objects = [{'type': 'picture', 'x': self.get_bbox().x + self._animation[0],
                    'y': self.get_bbox().y + self._animation[1], 'sprite_name': self._sprite_name}]
        information = {'depth': self._bbox.y + self._animation[1], 'main_sprite_name': self._sprite_name,
                       'objects': objects}
        return information

    def step(self, delta_t):
        pass

    def take(self, mine_speed):
        self._is_mining = True
        self._health -= mine_speed
        self._animation[0] += random.randint(-1, 1)
        self._animation[1] += random.randint(-1, 1)

    def is_mining(self):
        return self._is_mining

    def is_dead(self):
        return self._health < 0

    def get_y(self):
        return self._bbox.y

    def get_type(self):
        return self._name
