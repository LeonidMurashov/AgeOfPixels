from server.factory.unit import *


class Monitor:
    def __init__(self):
        pass

    def extract_information(self, objects, players, target_player):
        information = dict()
        information['objects'] = []
        information['players'] = target_player.get_player_info()

        for obj in objects:
            camera_coordinates = target_player.camera_coordinates
            object_info = obj.get_object_info()
            for i, element in enumerate(object_info['objects']):
                object_info['objects'][i]['x'] = element['x'] + camera_coordinates[0]
                object_info['objects'][i]['y'] = element['y'] + camera_coordinates[1]

            information['objects'].append(object_info)
        return information
