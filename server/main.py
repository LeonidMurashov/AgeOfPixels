import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath('__file__')))

from server.game import Game
from server.network_server_manager import NetworkServerManager
from server.configs import DEFAULT_PORT


def init_server():
    # sys.argv = ['main.py', '174.138.15.143']
    # sys.argv = ['main.py', '192.168.0.104']
    # Get command line argument of server ip
    if len(sys.argv) != 2:
        print('Usage:', sys.argv[0].split('/')[-1], 'host_ip')
        print('e.g.', sys.argv[0].split('/')[-1], '192.168.0.***')
        return
    else:
        host = sys.argv[1]
        return NetworkServerManager(localaddr=(host, int(DEFAULT_PORT)))


def main():
    network_manager = init_server()
    if not network_manager:
        return

    # Restarts game once game is finished
    while True:
        game = Game(network_manager)
        while game.step():
            pass

        network_manager.close()
        network_manager = init_server()

if __name__ == "__main__":
    sys.exit(main())
