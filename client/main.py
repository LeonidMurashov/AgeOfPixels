import contextlib

with contextlib.redirect_stdout(None):
    import pygame

import pygame.locals
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath('__file__')))
from client.selection_rect import *
from client.music_manager import MusicManager
from client.network_client_manager import NetworkClientManager
import time
from client.configs import *
from pygame import Rect
from client.renderer import Renderer
import random


def rect_to_list(r: Rect):
    return [r.x, r.y, r.w, r.h]

def process_events(selection_rect: SelectionRect, renderer: Renderer):
    events = []
    for event in pygame.event.get():
        if event.type == pygame.locals.QUIT or \
                (event.type == pygame.locals.KEYDOWN and event.key == pygame.locals.K_ESCAPE):
            return None
        elif event.type == pygame.locals.KEYDOWN:
            if event.key == pygame.locals.K_DELETE:
                events.append({'name': 'delete_button', 'params': []})
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:  # Left mouse button
                selection_rect.start_selection(event.pos)
                events.append({'name': 'left_click', 'params': [event.pos]})
            elif event.button == 3:  # Right mouse button
                events.append({'name': 'right_click', 'params': [event.pos]})
        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                selection_rect.finish_selection()
                events.append({'name': 'selection_rect_finished', 'params': [rect_to_list(selection_rect.get_rect())]})
        elif event.type == pygame.MOUSEMOTION:
            selection_rect.drag_selection(event.pos)

    # {'name': 'offset_camera', 'params': [[1, 1]]}
    position = pygame.mouse.get_pos()
    if position[0] == 0:
        events.append({'name': 'offset_camera', 'params': [[SCROLL_SENSETIVITY, 0]]})
    if position[1] == 0:
        events.append({'name': 'offset_camera', 'params': [[0, SCROLL_SENSETIVITY]]})
    if position[0] == renderer.screen_rect.w - 1:
        events.append({'name': 'offset_camera', 'params': [[-SCROLL_SENSETIVITY, 0]]})
    if position[1] == renderer.screen_rect.h - 1:
        events.append({'name': 'offset_camera', 'params': [[0, -SCROLL_SENSETIVITY]]})
    return events


def lobby():
    # server_ip = input('Enter local server IP: ')
    # server_ip = '192.168.0.104'
    name = input('Enter player name: ')
    print('Connecting to the server...')
    network_manager = NetworkClientManager(SERVER_IP, DEFAULT_PORT, name)
    print('Connected.')
    players_number = int(input('Enter players number: '))
    network_manager.SendPlayersNumber(players_number)

    print('Waiting for players to connect...')
    while not network_manager.IsGameStarted() and network_manager.GetFrameData() == {}:
        network_manager.Update()
        time.sleep(0.01)

    print('Starting the game!')
    return network_manager, name


def main():

    # Lobby in the beginning; waits for players to connect
    network_manager, name = lobby()

    renderer = Renderer(name)
    selection_rect = SelectionRect(renderer)
    music_manager = MusicManager()

    clock = pygame.time.Clock()

    while True:
        elapsed_time = clock.tick_busy_loop() / 1000

        # Extracts user input events
        events = process_events(selection_rect, renderer)
        if events is None:
            return

        # Connects to the server: sends events data and recieves unit's position, etc...
        network_manager.SendEvents(events)
        network_manager.Update()
        frame_data = network_manager.GetFrameData()

        # Renders units given info from server
        renderer.render_frame_data(frame_data)

        # UI; Renders button menu at the bottom and fps
        renderer.render_fps(int(clock.get_fps()))

        ping = int(network_manager.GetPing() * 1000)
        renderer.render_ping(ping)

        # Selection rect management
        if selection_rect.is_selection_active():
            selection_rect.render()

        # Needed to update frame
        renderer.finish_frame()


if __name__ == "__main__":
    sys.exit(main())
