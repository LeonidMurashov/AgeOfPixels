import os
from client.configs import *
import platform
import pygame
from pygame import Rect

if platform.system() in ['Windows']:
    import ctypes
    from ctypes import windll


class Renderer:
    images = dict()

    def __init__(self, name):
        os.environ['SDL_VIDEO_WINDOW_POS'] = "%d, %d" % (0, -1)
        pygame.init()
        self._name = name
        self.comic_sans = pygame.font.SysFont('Comic Sans MS', 26)
        info_object = pygame.display.Info()
        screen_rect = Rect(0, 0, info_object.current_w, info_object.current_h)

        if platform.system() in ['Windows']:
            # Fix bug with text scaling in Windows
            ctypes.windll.user32.SetProcessDPIAware()
            true_res = (windll.user32.GetSystemMetrics(0), windll.user32.GetSystemMetrics(1))
        elif platform.system() in ['Linux', 'Darwin']:
            true_res = (screen_rect.w, screen_rect.h)
        else:
            true_res = USUAL_SCREEN_SIZE

        self.screen: pygame.Surface = pygame.display.set_mode(true_res, pygame.FULLSCREEN)
        pygame.display.set_caption("AgeOfPixels")

        self.load_images()

    def load_images(self):
        for file in os.listdir(IMAGES_FOLDER):
            self.images[file] = pygame.image.load(os.path.join(IMAGES_FOLDER, file)).convert_alpha()

            if file not in ['sand.jpg', 'grass.jpg', 'menu.bmp']:
                self.images[file] = pygame.transform.scale(
                    self.images[file],
                    (
                        self.images[file].get_size()[0] * PIXEL_SCALE,
                        self.images[file].get_size()[1] * PIXEL_SCALE
                    )
                )

    def render_frame_data(self, frame_data):
        if 'objects' not in frame_data:
            return

        objects = frame_data['objects']
        objects = sorted(objects, key=lambda x: x['depth'])

        for obj in objects:
            object_image: pygame.Surface = self.images[obj['main_sprite_name']]
            for element in obj['objects']:
                if element['type'] == 'picture':
                    part_image: pygame.Surface = self.images[element['sprite_name']]
                    self.screen.blit(
                        part_image,
                        (
                            element['x'] - part_image.get_width() / 2,
                            element['y'] - part_image.get_height()
                        )
                    )

                elif element['type'] == 'selection_ellipsis' and obj['owner_name'] == self._name:
                    pygame.draw.ellipse(
                        self.screen,
                        (255, 255, 255),
                        Rect(
                            element['x'] - (object_image.get_width() * element['width_ratio_with_image']) / 2,
                            element['y'] - 5,
                            object_image.get_width() * element['width_ratio_with_image'],
                            10
                        ),
                        2)

                elif element['type'] == 'rectangle':
                    pygame.draw.rect(
                        self.screen,
                        element['color'],
                        Rect(element['x'],
                             element['y'],
                             element['w'],
                             element['h']))

                elif element['type'] == 'health_bar' and obj['owner_name'] == self._name:

                    # Red part
                    pygame.draw.rect(self.screen,
                                     (255, 0, 0),
                                     Rect(element['x'] - 30 / 2,
                                          element['y'] - object_image.get_height() - 10,
                                          30,
                                          2))
                    # Green part
                    pygame.draw.rect(self.screen,
                                     (0, 255, 0),
                                     Rect(element['x'] - 30 / 2,
                                          element['y'] - object_image.get_height() - 10,
                                          30 * element['health_ratio'],
                                          2))

        self.render_menu(frame_data['players'])

    def render_menu(self, player_info):
        menu_image = self.images['menu.bmp']
        self.screen.blit(
            menu_image,
            (
                0,
                self.screen_rect.h - menu_image.get_height()
            )
        )

        text = self.comic_sans.render('Type:', True, (255, 255, 0))
        self.screen.blit(text, (50, self.screen_rect.h - menu_image.get_height() / 2 - 26))

        text_resources = self.comic_sans.render('Resources:', True, (255, 255, 0))
        self.screen.blit(text_resources, (1500, self.screen_rect.h - menu_image.get_height() / 2 - 26))

        resources = player_info['resources']
        text_position = 5
        for i in resources.items():
            text_1 = self.comic_sans.render(str(i), True, (255, 255, 0))
            self.screen.blit(text_1, (1650, self.screen_rect.h - 35 * text_position + 24))
            text_position -= 1

        selected_sprite_name = player_info['menu']['selected_type']
        type_text = self.comic_sans.render(selected_sprite_name, True, (255, 255, 0))
        self.screen.blit(type_text, (120, self.screen_rect.h - menu_image.get_height() / 2 - 26))

        for menu_obj in player_info['menu']['upgrades']:
            pygame.draw.rect(
                self.screen,
                menu_obj['color'],
                Rect(menu_obj['x'],
                     menu_obj['y'],
                     menu_obj['w'],
                     menu_obj['h']))
            button_text = menu_obj['name']
            button_text_to_render = self.comic_sans.render(button_text, True, (255, 255, 0))
            self.screen.blit(button_text_to_render,
                             (menu_obj['x'] + 10, menu_obj['y'] + menu_obj['h'] / 2 - 20))

    def render_fps(self, fps):
        if fps < 30:
            text = self.comic_sans.render('FPS: {} - very bad :('.format(fps), True, (255, 255, 0))
        else:
            text = self.comic_sans.render('FPS: {}'.format(fps), True, (255, 255, 0))

        self.screen.blit(text, (10, 0))

    def render_ping(self, ping):
        if ping > USER_DELAY_TIME * 1000:
            text = self.comic_sans.render('PING: {} ms - very bad :('.format(ping), True, (255, 0, 0))
        else:
            text = self.comic_sans.render('PING: {} ms'.format(ping), True, (255, 255, 0))

        self.screen.blit(text, (10, 30))

    def finish_frame(self):
        pygame.display.flip()
        pygame.time.delay(1)
        self.screen.fill((140, 140, 140))

    @property
    def screen_rect(self):
        return self.screen.get_rect()
