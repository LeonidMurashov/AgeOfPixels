from client.configs import *
import os
import pygame


class MusicManager:
    def __init__(self):
        # start playing music
        pygame.mixer.music.load(os.path.join(MUSIC_FOLDER, 'background_music.mp3'))
        pygame.mixer.music.play(-1)
        pygame.mixer.music.set_volume(0.1)
