from PodSixNet.Connection import connection, ConnectionListener
from _thread import *
import time
from collections import deque
from client.configs import *


class NetworkClientManager(ConnectionListener):
    _frame_data_time_list = []
    _players_order = []
    _game_started = False
    _last_ping_window = deque([0.02], maxlen=100)
    _last_contact_time = 0

    def __init__(self, host, port, player_name):
        self.Connect((host, port))
        self.player_name = player_name
        connection.Send({"action": "player_name", "player_name": player_name})
        # t = start_new_thread(self.InputLoop, ())

    # This method uses delayed time trick - user experience is delayed by USER_DELAY_TIME
    # https://habr.com/ru/post/450574/?utm_source=vk&utm_medium=social&utm_campaign=sozdanie-mnogopolzovatelskoy-veb-igry
    def GetFrameData(self):
        if not self._frame_data_time_list:
            return dict()

        current_time = time.clock()
        for i, frame_data in enumerate(reversed(self._frame_data_time_list)):
            if current_time - frame_data['time_stamp'] < USER_DELAY_TIME and i != len(self._frame_data_time_list) - 1:
                continue
            else:
                del self._frame_data_time_list[: len(self._frame_data_time_list) - i - 1]
                return frame_data['data']

    def Update(self):
        connection.Pump()
        self.Pump()

    def IsGameStarted(self):
        return self._game_started

    def SendPlayersNumber(self, players_number):
        connection.Send({"action": "set_players_number", "players_number": players_number})

    def SendEvents(self, events):
        if events:
            connection.Send({"action": "events", "events": events})

    def GetPing(self):
        return max(self._last_ping_window)

    ####################################################################################
    # Network event/message callbacks                                                  #
    # Methods down here are called by PodSixNet when it receives messages from network #
    ####################################################################################

    def Network_frame_data(self, frame_data):
        current_time = time.clock()

        self._frame_data_time_list.append({'time_stamp': current_time, 'data': frame_data['data']})

        # Write down data for ping info
        self._last_ping_window.append(current_time - self._last_contact_time)
        self._last_contact_time = current_time

    def Network_game_started(self, data):
        self._game_started = True

    # Built-in stuff

    def Network_connected(self, data):
        print("You are now connected to the server")

    def Network_error(self, data):
        print('error:', data['error'][1])
        connection.Close()

    def Network_disconnected(self, data):
        print('Server disconnected')
        exit()
